import { createStore, applyMiddleware, combineReducers } from "redux";
import { cakeReducer } from "./cake/cakeReducer";
import logger from "redux-logger";
import thunk from "redux-thunk";
import { todoReducer } from "./todo/todoReducer";

const rootReducer = combineReducers({ cake: cakeReducer, todo: todoReducer });

const store = createStore(rootReducer, applyMiddleware(logger, thunk));

export default store;
