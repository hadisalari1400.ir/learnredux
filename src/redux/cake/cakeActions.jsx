import { ADD_CAKE, DELETE_CAKE } from "./cakeTypes";

export const addCake = () => {
  return { type: ADD_CAKE };
};

export const deleteCake = (data) => {
  return { type: DELETE_CAKE, payload: data };
};
