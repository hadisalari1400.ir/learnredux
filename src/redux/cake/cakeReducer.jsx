// reducer

import { ADD_CAKE, DELETE_CAKE } from "./cakeTypes";

const initialState = {
  cake: 10,
};

export const cakeReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_CAKE:
      return { cake: state.cake + 1 };

    case DELETE_CAKE:
      return { cake: state.cake - 1 };

    default:
      return state;
  }
};
