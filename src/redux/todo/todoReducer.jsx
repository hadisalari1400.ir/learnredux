// reducer

import { FETCH_FAILER, FETCH_REQUEST, FETCH_SUCCESS } from "./todoTypes";

const initialState = {
  loading: false,
  todos: [],
  error: "",
};

export const todoReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_REQUEST:
      return { ...state, loading: true };

    case FETCH_SUCCESS:
      return { loading: false, todos: action.payload, error: "" };

    case FETCH_FAILER:
      return {
        loading: false,
        todos: [],
        error: action.payload,
      };

    default:
      return state;
  }
};
