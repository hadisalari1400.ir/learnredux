// action creator

import { FETCH_FAILER, FETCH_REQUEST, FETCH_SUCCESS } from "./todoTypes";
import axios from "axios";

const fetchRequest = () => {
  return { type: FETCH_REQUEST };
};

const fetchSuccess = (data) => {
  return { type: FETCH_SUCCESS, payload: data };
};

const fetchFailer = (err) => {
  return { type: FETCH_FAILER, payload: err };
};

// async action creator

export const fetchTodos = () => async (dispatch) => {
  dispatch(fetchRequest());
  console.log("Hi");
  try {
    const { data } = await axios.get("http://localhost:3001/todos");
    dispatch(fetchSuccess(data));
  } catch (err) {
    dispatch(fetchFailer(err.message));
  }
};

export const addTodo = (value) => async (dispatch) => {
  dispatch(fetchRequest());
  try {
    await axios.post("http://localhost:3001/todos", value);
    const { data } = await axios.get("http://localhost:3001/todos");
    dispatch(fetchSuccess(data));
  } catch (err) {
    dispatch(fetchFailer(err.message));
  }
};

export const deleteTodo = (id) => async (dispatch) => {
  dispatch(fetchRequest());
  try {
    await axios.delete(`http://localhost:3001/todos/${id}`);
    const { data } = await axios.get("http://localhost:3001/todos");
    dispatch(fetchSuccess(data));
  } catch (err) {
    dispatch(fetchFailer(err.message));
  }
};

export const editTodo =
  ({ id, title, completed }) =>
  async (dispatch) => {
    dispatch(fetchRequest());
    try {
      await axios.put(`http://localhost:3001/todos/${id}`, {
        id,
        title,
        completed: !completed,
      });
      const { data } = await axios.get("http://localhost:3001/todos");
      dispatch(fetchSuccess(data));
    } catch (err) {
      dispatch(fetchFailer(err.message));
    }
  };
