import { Provider } from "react-redux";
// import Counter from "./features/counter/Counter";

// const App = () => {
//   return (
//     <Provider store={store}>
//       <Counter />
//     </Provider>
//   );
// };

// export default App;

import "./App.css";

import "bootstrap/dist/css/bootstrap.min.css";
import AddTodoForm from "./components/Todos/AddTodoForm";
import TodoList from "./components/Todos/TodoList";
import store from "./redux/store";
import Cakes from "./components/Cakes/Cake";

const App = () => {
  return (
    <Provider store={store}>
      <div className="container">
        <h1>hi Hadi !</h1>

        <Cakes />

        <div>
          <AddTodoForm />
          <TodoList />
        </div>
      </div>
    </Provider>
  );
};

export default App;
