import React from "react";
import { deleteTodo, editTodo } from "../../redux/todo/todoActions";

const TodoItem = ({ id, title, completed, dispatch }) => {
  return (
    <li className={`list-group-item ${completed && "list-group-item-success"}`}>
      <div className="d-flex justify-content-between">
        <span className="d-flex align-items-center">
          <input
            type="checkbox"
            className="mr-3"
            checked={completed}
            onChange={() => dispatch(editTodo({ id, title, completed }))}
          ></input>
          {title}
        </span>
        <button
          className="btn btn-danger"
          onClick={() => dispatch(deleteTodo(id))}
        >
          Delete
        </button>
      </div>
    </li>
  );
};

export default TodoItem;
