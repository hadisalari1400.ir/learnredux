import { useDispatch, useSelector } from "react-redux";
import { addCake, deleteCake } from "../../redux/cake/cakeActions";

const Cakes = () => {
  const { cake } = useSelector((state) => state.cake);
  const dispatch = useDispatch();

  return (
    <div>
      <h4>number of cakes : {cake} </h4>
      <button onClick={() => dispatch(addCake())}>+</button>
      <button onClick={() => dispatch(deleteCake())}>-</button>
    </div>
  );
};

export default Cakes;
